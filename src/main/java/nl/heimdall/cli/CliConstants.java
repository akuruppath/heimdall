/**
 * CliConstants.java - list of all constants related to the command line options.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.cli;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * This class contains a list of all the constants that are related to the
 * command line operations.
 *
 * @author ajay
 *
 */

public class CliConstants {

	public static final String DBTYPE = "dbType";
	public static final String EMAIL = "email";
	public static final String SEPARATOR = "separator";
	public static final String QUOTE = "quote";
	public static final String ESCAPE = "escape";
	public static final String LINEEND = "lineend";

	public static final String DBTYPE_POSTGRES = "postgres";
	public static final String DBTYPE_MYSQL = "mysql";
	public static final String DBTYPE_MSSQL = "mssql";
	public static final String DBTYPE_H2 = "h2";

	public static final String QUOTETYPE_SINGLE = "'";
	public static final String QUOTETYPE_DOUBLE = "\"";
	public static final String LINEEND_UNIX = "\n";
	public static final String LINEEND_WINDOWS = "\r\n";
	public static final String ENABLE_EMAIL = "yes";
	public static final String DISABLE_EMAIL = "no";
	public static final String DEFAULT_SEPARATOR = ",";
	public static final String DEFAULT_ESCAPE = "/";
	public static final String ARGVAL_SINGLE_QUOTE = "single";
	public static final String ARGVAL_DOUBLE_QUOTE = "double";
	public static final String ARGVAL_LINEEND_UNIX = "unix";
	public static final String ARGVAL_LINEEND_WINDOWS = "windows";

	public static final String DB_TYPE_DESCRIPTION = "Required argument. Available options: "
			+ DBTYPE_POSTGRES
			+ ", "
			+ DBTYPE_MYSQL
			+ ", "
			+ DBTYPE_MSSQL
			+ "\neg. usage: --dbtype=postgres";
	public static final String DB_TYPE_MSG = "Required argument. A database type has to be specified. Available options: "
			+ DBTYPE_POSTGRES + ", " + DBTYPE_MYSQL + ", " + DBTYPE_MSSQL;

	public static final String DB_TYPE_OPT = "dbtype";
	public static final String EMAIL_TYPE_OPT = "email";
	public static final String EMAIL_OPT_YES = "yes";
	public static final String EMAIL_TYPE_DESCRIPTION = "If email has to be sent or not. Available options: yes/no. Defaults to no. "
			+ "\neg. usage: --email=yes";
	public static final String EMAIL_TYPE_MSG = "Non-mandatory argument. To enable email, specify as --email=yes. Or leave it out altogether. Available options: yes/no. Defaults to no. "
			+ " Leaving this field out altogether or specifying --email=no has the same effect.";

	public static final String ESCAPE_DESCRIPTION = "Non-mandatory argument. The escape character to be used in the CSV files. "
			+ "\neg. usage: --escape=<escape-char>";
	public static final String ESCAPE_TYPE_MSG = "Non-mandatory argument. To enable escape character, specify as --escape=<escape-char>. Or leave it out altogether.";
	public static final String ESCAPE_TYPE_OPT = "escape";

	public static final String HELP_OPTION_DESCRIPTION = "Print this message and exit.";
	public static final String HELP_OPTION_OPT = "help";

	public static final String LINE_ENDING_DESCRIPTION = "Non-mandatory argument. The line-ending character to be used in the CSV files. Available options: unix/windows. Defaults to unix. "
			+ "\neg. usage: --lineending=unix";
	public static final String LINE_ENDING_TYPE_MSG = "Non-mandatory argument. To specify lineending, specify as --lineending=<line-ending>. Or leave it out altogether. Available options: unix/windows. Defaults to unix.";
	public static final String LINE_ENDING_TYPE_OPT = "lineending";

	public static final String SEPARATOR_DESCRIPTION = "Non-mandatory argument. The separator character to be used in the CSV files. Available options can be anything. Defaults to comma character. "
			+ "\neg. usage: --separator=$";
	public static final String SEPARATOR_TYPE_MSG = "Non-mandatory argument. To specify separator, specify as  --separator=<separator-char>. Or leave it out altogether. Can be anything. Defaults to comma character.";
	public static final String SEPARATOR_TYPE_OPT = "separator";

	public static final String QUOTE_DESCRIPTION = "Non-mandatory argument. The quote character to be used in the CSV files. Available options: single/double. Defaults to double. "
			+ "\neg. usage: --lineending=single";
	public static final String QUOTE_TYPE_MSG = "Non-mandatory argument. To specify a quote-char, specify as --quote=<quote-char>. Or leave it out altogether. Available options: single/double. Defaults to double.";
	public static final String QUOTE_TYPE_OPT = "quote";

	public static final String VERSION_OPTION_DESCRIPTION = "Print the version information and exit.";
	public static final String VERSION_OPTION_OPT = "version";

  public static final String PROGRAM_NAME = " heimdall";
	public static final String PROGRAM_LICENSE = " \n"
			+ " Copyright (C) 2014  Ajay Kuruppath\n\n"
			+

			" This library is free software; you can redistribute it and/or"
			+ "\n"
			+ " modify it under the terms of the GNU Lesser General Public"
			+ "\n"
			+ " License as published by the Free Software Foundation; either"
			+ "\n"
			+ " version 2.1 of the License, or (at your option) any later version."
			+ "\n\n"
			+

			" This library is distributed in the hope that it will be useful,"
			+ "\n"
			+ " but WITHOUT ANY WARRANTY; without even the implied warranty of"
			+ "\n"
			+ " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU"
			+ "\n"
			+ " Lesser General Public License for more details."
			+ "\n\n"
			+

			" You should have received a copy of the GNU Lesser General Public"
			+ "\n"
			+ " License along with this library; if not, write to the Free Software"
			+ "\n"
			+ " Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA"
			+ "\n";

	public static final String VERSION_MSG = "\n" + PROGRAM_NAME + " v1.0\n"
			+ PROGRAM_LICENSE;

	public static final Set<String> supportedDbOptions = new HashSet<String>(
			Arrays.asList(new String[] { DBTYPE_POSTGRES, DBTYPE_MYSQL,
					DBTYPE_MSSQL, DBTYPE_H2 }));

	public static final Set<String> supportedQuoteOptions = new HashSet<String>(
			Arrays.asList(new String[] { ARGVAL_SINGLE_QUOTE,
					ARGVAL_DOUBLE_QUOTE }));

	public static final Set<String> supportedLineEndOptions = new HashSet<String>(
			Arrays.asList(new String[] { ARGVAL_LINEEND_UNIX,
					ARGVAL_LINEEND_WINDOWS }));

	public static final Set<String> supportedEmailOptions = new HashSet<String>(
			Arrays.asList(new String[] { ENABLE_EMAIL, DISABLE_EMAIL }));

}
