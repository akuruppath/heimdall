/**
 * OptionParser.java - consists of range of methods that parses the CLI options.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.cli;

import main.java.nl.heimdall.job.Job;
import java.util.HashMap;
import java.util.Map;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.log4j.Logger;

/**
 * This class consists of methods that are responsible for parsing the command
 * line options and check if they are legal or not.
 * 
 * @author ajay
 * 
 */

public class OptionParser {

	private static Logger LOG = Logger.getLogger(OptionParser.class);

	private static final CommandLineParser parser = new GnuParser();
	private static final Map<String, String> optionMap = new HashMap<String, String>();
	private static final Options options = new ReportOptions()
			.getOptions();

	/**
	 * Main method of the program.
	 * 
	 * @param args
	 *            the arguments supplied from the command line.
	 */
	public static void main(String[] args) {
		OptionParser.parseReportOptions(args);
		new Job().execute();
	}

	/**
	 * Method to retrieve an option from the option-map. If an unknown option or
	 * a null is passed then it exits the program.
	 * 
	 * @param key
	 *            that represents the option being searched for
	 * @return String the value that corresponds to the CLI key that was sent as
	 *         the argument.
	 */
	public static String getOption(String key) {
		if (key == null || optionMap.get(key) == null
				|| !optionMap.containsKey(key)) {
			LOG.warn("Unknown key recieved : " + key
					+ " Was this option set ???");
			return null;
		}
		return optionMap.get(key);
	}

	/**
	 * Method to parse the command-line arguments. Depending on the arguments
	 * passed at the command line, it does different actions.
	 * 
	 * @params args the arguments supplied from the command-line.
	 * 
	 */
	// TODO: break this down and make it more neater.
	private static void parseReportOptions(String[] args) {
		if (args.length <= 0) {
			printHelp();
		}
		try {
			final CommandLine line = parser.parse(options, args);
			// help
			if (line.hasOption(CliConstants.HELP_OPTION_OPT)) {
				printHelp();
			}
			if (line.hasOption(CliConstants.VERSION_OPTION_OPT)) {
				System.err.println(CliConstants.VERSION_MSG);
				System.exit(0);
			}
			// dbtype
			extractedDbOption(line);
			// email
			extractedEmailOption(line);
			// separator
			extractedSeparatorOption(line);
			// quote
			extractedQuoteOption(line);
			// escape-char
			extractedEscapeOption(line);
			// line-end
			extractedLineEndOption(line);

		} catch (final UnrecognizedOptionException e) {
			throw new IllegalArgumentException(
					"Error recognizing an argument: " + e.getMessage(), e);
		} catch (final ParseException e) {
			throw new IllegalArgumentException("Error parsing an argument : "
					+ e.getMessage(), e);
		}
	}

	private static void extractedDbOption(final CommandLine line) {
		final String dbType = line.getOptionValue(CliConstants.DB_TYPE_OPT);
		if (dbType == null || dbType.isEmpty()
				|| !CliConstants.supportedDbOptions.contains(dbType)) {
			throw new IllegalArgumentException(CliConstants.DB_TYPE_MSG);
		} else {
			optionMap.put(CliConstants.DBTYPE, dbType);
		}
	}

	private static void extractedEmailOption(final CommandLine line) {
		final String email = line.getOptionValue(CliConstants.EMAIL_TYPE_OPT);
		if (email == null) {
			// default line ending char
			optionMap.put(CliConstants.EMAIL, CliConstants.DISABLE_EMAIL);
			return;
		}
		if (email != null) {
			if (email.isEmpty()) {
				throw new IllegalArgumentException(CliConstants.EMAIL_TYPE_MSG);
			} else {
				optionMap.put(CliConstants.EMAIL, email);
			}
		}
	}

	private static void extractedSeparatorOption(final CommandLine line) {
		final String separator = line
				.getOptionValue(CliConstants.SEPARATOR_TYPE_OPT);
		if (separator == null) {
			// set default separator char
			optionMap.put(CliConstants.SEPARATOR,
					CliConstants.DEFAULT_SEPARATOR);
			return;
		}
		if (separator.isEmpty()) {
			throw new IllegalArgumentException(CliConstants.SEPARATOR_TYPE_MSG);
		} else {
			optionMap.put(CliConstants.SEPARATOR, separator);
		}
	}

	private static void extractedQuoteOption(final CommandLine line) {
		final String quote = line.getOptionValue(CliConstants.QUOTE_TYPE_OPT);
		if (quote == null) {
			// default line ending char
			optionMap.put(CliConstants.QUOTE, CliConstants.QUOTETYPE_DOUBLE);
			return;
		} else if (quote != null) {
			if (quote.isEmpty()
					|| !CliConstants.supportedQuoteOptions.contains(quote)) {
				throw new IllegalArgumentException(CliConstants.QUOTE_TYPE_MSG);
			} else {
				final String option = quote
						.equals(CliConstants.ARGVAL_SINGLE_QUOTE) ? CliConstants.QUOTETYPE_SINGLE
						: CliConstants.QUOTETYPE_DOUBLE;
				optionMap.put(CliConstants.QUOTE, option);
			}
		}
	}

	private static void extractedEscapeOption(final CommandLine line) {
		final String escape = line.getOptionValue(CliConstants.ESCAPE_TYPE_OPT);
		if (escape == null) {
			// set default escape char
			optionMap.put(CliConstants.ESCAPE, CliConstants.DEFAULT_ESCAPE);
			return;
		}
		if (escape != null) {
			if (escape.isEmpty()) {
				throw new IllegalArgumentException(CliConstants.ESCAPE_TYPE_MSG);
			} else {
				optionMap.put(CliConstants.ESCAPE, escape);
			}
		}
	}

	private static void extractedLineEndOption(final CommandLine line) {
		final String lineend = line
				.getOptionValue(CliConstants.LINE_ENDING_TYPE_OPT);
		if (lineend == null) {
			// default line ending char
			optionMap.put(CliConstants.LINEEND, CliConstants.LINEEND_UNIX);
			return;
		} else if (lineend != null) {
			if (lineend.isEmpty()
					|| !CliConstants.supportedLineEndOptions.contains(lineend)) {
				throw new IllegalArgumentException(
						CliConstants.LINE_ENDING_TYPE_MSG);
			} else {
				final String option = lineend
						.equals(CliConstants.ARGVAL_LINEEND_UNIX) ? CliConstants.LINEEND_UNIX
						: CliConstants.LINEEND_WINDOWS;
				optionMap.put(CliConstants.LINEEND, option);
			}
		}
	}

	/**
	 * Method that prints the help message, if no arguments were specified at
	 * the command-line.
	 * 
	 */
	private static void printHelp() {
		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(CliConstants.PROGRAM_NAME, options);
		System.exit(0);
	}
}
