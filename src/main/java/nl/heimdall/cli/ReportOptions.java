/**
 * ReportOptions.java - list of functions to parse and use command line arguments.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;

/**
 * Class that contains methods responsible for : <code>
 * - creating command-line options.
 * - initialise the command-line options.
 * - parsing the command-line arguments.
 * </code>
 *
 * @author ajay
 *
 */

public final class ReportOptions {

	private static Logger LOG = Logger.getLogger(ReportOptions.class);

	private static Options options;

	static {
		initializeReportOptions();
	}

	  /**
   * @return options object which contains all the options available for the
   *         report.
   */
	public Options getOptions() {
		return options;
	}

	/**
	 * Method to create the options required for the program and initializes the
	 * same.
	 */
	private static void initializeReportOptions() {
		final Option help = new Option(CliConstants.HELP_OPTION_OPT,
				CliConstants.HELP_OPTION_DESCRIPTION);
		final Option versionOption = new Option(
				CliConstants.VERSION_OPTION_OPT,
				CliConstants.VERSION_OPTION_DESCRIPTION);
		@SuppressWarnings("static-access")
		final Option dbType = OptionBuilder.withArgName("db").hasArgs(1)
				.isRequired(false)
				.withDescription(CliConstants.DB_TYPE_DESCRIPTION)
				.create(CliConstants.DB_TYPE_OPT);
		@SuppressWarnings("static-access")
		final Option email = OptionBuilder.withArgName("email")
				.hasArgs(1).isRequired(false)
				.withDescription(CliConstants.EMAIL_TYPE_DESCRIPTION)
				.create(CliConstants.EMAIL_TYPE_OPT);
		@SuppressWarnings("static-access")
		final Option separator = OptionBuilder.withArgName("separator")
				.hasArgs(1).isRequired(false)
				.withDescription(CliConstants.SEPARATOR_DESCRIPTION)
				.create(CliConstants.SEPARATOR_TYPE_OPT);
		@SuppressWarnings("static-access")
		final Option quote = OptionBuilder.withArgName("quote").hasArgs(1)
				.isRequired(false)
				.withDescription(CliConstants.QUOTE_DESCRIPTION)
				.create(CliConstants.QUOTE_TYPE_OPT);
		@SuppressWarnings("static-access")
		final Option escape = OptionBuilder.withArgName("escape").hasArgs(1)
				.isRequired(false)
				.withDescription(CliConstants.ESCAPE_DESCRIPTION)
				.create(CliConstants.ESCAPE_TYPE_OPT);
		@SuppressWarnings("static-access")
		final Option lineend = OptionBuilder.withArgName("line-ending")
				.hasArgs(1).isRequired(false)
				.withDescription(CliConstants.LINE_ENDING_DESCRIPTION)
				.create(CliConstants.LINE_ENDING_TYPE_OPT);
		options = new Options();
		options.addOption(help);
		options.addOption(versionOption);
		options.addOption(dbType);
		options.addOption(email);
		options.addOption(separator);
		options.addOption(quote);
		options.addOption(escape);
		options.addOption(lineend);
		LOG.info("Initialized Reporting options successfully.");
	}

}
