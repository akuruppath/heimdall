/**
 * Constants.java - list of constants being used in the program.
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.constants;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class defines the constants being used.
 * 
 * @author ajay
 * 
 */

public class Constants {

	public static final String DEFAULT_CSV_PATH = System
			.getProperty("user.dir") + System.getProperty("file.separator");

	public static final String DEFAULT_CSV_FILE = DEFAULT_CSV_PATH
			+ "jdbReport-"
			+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'.txt'")
					.format(new Date()) + ".csv";

	public static final String DEFAULT_EMAIL_ADDRESS_SEPARATOR = ",";

	public static final String JDBC_DRIVER = "JDBC_DRIVER";
	public static final String JDBC_PASSWORD = "JDBC_PASSWORD";
	public static final String JDBC_URL = "JDBC_URL";
	public static final String JDBC_USERNAME = "JDBC_USERNAME";

	public static final String NODE_CODE = "code";
	public static final String NODE_CSV_NAME = "csv_name";
	public static final String NODE_DB_DRIVER = "db_driver";
	public static final String NODE_DB_PASSWORD = "db_password";
	public static final String NODE_DB_URL = "db_url";
	public static final String NODE_DB_USER = "db_user";

}
