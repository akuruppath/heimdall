/**
 * DbUtils.java - loads the constants.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.db;

import main.java.nl.heimdall.constants.Constants;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import org.apache.log4j.Logger;
import org.dom4j.Element;

/**
 * Class that is responsible for loading the database related stuff.
 * 
 * @author ajay
 * 
 */

public class DbUtils {

	private static Logger LOG = Logger.getLogger(DbUtils.class);

	private static Element dbElement;
	private static final Map<String, String> dbConfigMap = new HashMap<String, String>();

	/**
	 * Method that returns a map with the required parameters.
	 * 
	 * @return HashMap<String, String>()
	 */
	public static Map<String, String> getDbConfigMap() {
		if (dbConfigMap.isEmpty()) {
			loadConfig();
		}
		return dbConfigMap;
	}

	/**
	 * Method that loads a map with all the required constants.
	 */
	private static void loadConfig() {
		try {
			dbElement = DbXmlParser.getDataBaseConfig();
			dbConfigMap.put(Constants.JDBC_DRIVER,
					dbElement.elementText(Constants.NODE_DB_DRIVER));
			dbConfigMap.put(Constants.JDBC_USERNAME,
					dbElement.elementText(Constants.NODE_DB_USER));
			dbConfigMap.put(Constants.JDBC_PASSWORD,
					dbElement.elementText(Constants.NODE_DB_PASSWORD));
			dbConfigMap.put(Constants.JDBC_URL,
					dbElement.elementText(Constants.NODE_DB_URL));
		} catch (final IOException e) {
			LOG.error(
					"An exception happened while fetching the db configuration.",
					e);
		}
	}

}
