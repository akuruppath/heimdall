/**
 * DbXmlParser.java - range of methods related to getting info from db.xml.
 *
 * Copyright (C) 2014  Ajay Kuruppath
 * Copyright (C) 2014  West Consulting

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.db;

import main.java.nl.heimdall.utils.XmlUtils;
import main.java.nl.heimdall.cli.CliConstants;
import main.java.nl.heimdall.cli.OptionParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


import org.apache.log4j.Logger;
import org.dom4j.Element;

/**
 * This class consists of methods that are responsible for parsing the db.xml
 * and getting information from that.
 * 
 * @author ajay
 * 
 */

public class DbXmlParser {

	private static Logger LOG = Logger.getLogger(DbXmlParser.class);

	private static String DATABASE_PATH = "//databases/database/";
	private static final String DB_XML = "db.xml";

	static {
		DATABASE_PATH += OptionParser.getOption(CliConstants.DBTYPE);
		LOG.debug("Database path set to : " + DATABASE_PATH);
	}

	    /**
     * Method that is responsible for returning the db configuration.
     * 
     * @return database path
     * @throws IOException
     *             throws an IOException if the XML file cannot be opened
     *             properly.
     */
	public static Element getDataBaseConfig() throws IOException {
		return XmlUtils.getXmlNode(getDbFile(), DATABASE_PATH);
	}

	    /**
     * Method to return the input-stream that corresponds to the db.xml.
     * 
     * @return a stream
     */
	private static FileInputStream getDbFile() {
		FileInputStream stream = null;
		try {
			final File jarPath = new File(XmlUtils.class.getProtectionDomain()
					.getCodeSource().getLocation().getPath());
			final String propertiesPath = jarPath.getParentFile()
					.getAbsolutePath();
			stream = new FileInputStream(propertiesPath + File.separator
					+ DB_XML);
		} catch (final IOException e1) {
			e1.printStackTrace();
		}
		return stream;
	}

}
