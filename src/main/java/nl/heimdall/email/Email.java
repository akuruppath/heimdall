/**
 * Email.java - consists of range of methods related to sending email.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.email;

import main.java.nl.heimdall.constants.Constants;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


import org.apache.log4j.Logger;
import org.dom4j.Element;

/**
 * This class consists of methods related to sending an email.
 * 
 * @author ajay
 * 
 */

public class Email {

	private static Logger LOG = Logger.getLogger(Email.class);
	private static final Properties properties = System.getProperties();

	/**
	 * Method to send email message to recipients.
	 * 
	 * @param queryCsvMap
	 *            the map which contains the query and the file in which the
	 *            query results are written into.
	 */
	public void sendEmail(Map<String, String> queryCsvMap) {
		properties.setProperty(EmailConstants.SMTP_HOST_PROPERTY, getHost());
		properties.setProperty(EmailConstants.SMTP_PORT, getPort());
		final Session session = Session.getDefaultInstance(properties);
		LOG.trace("Got session : " + session);
		try {
			final Message message = new MimeMessage(session);
			writeEnvelope(message);
			// set the email-body content
			final Message emailMessage = fillEmailBody(message, queryCsvMap);
			// send email
			Transport.send(emailMessage);
			LOG.info("Sent email...");
		} catch (final MessagingException e) {
			LOG.error("Exception sending email", e);
		}
	}

	/**
	 * Method to set the query and its results in the email.
	 * 
	 * @param message
	 *            the message object onto which the default text has to be set.
	 * @param queryCsvMap
	 *            the map which contains the queries and the file that has the
	 *            results of those queries.
	 * @param multiPart
	 *            the multipart object into which each message part is added
	 * @throws MessagingException
	 *             throws a MessagingException if an exception occurs while
	 *             filling the message object.
	 */
	private void fillCsvData(Message message, Map<String, String> queryCsvMap,
			final Multipart multiPart) throws MessagingException {
		for (final Map.Entry<String, String> entry : queryCsvMap.entrySet()) {
			final BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(entry.getKey());
			multiPart.addBodyPart(messageBodyPart);
			setAttachmentPart(multiPart, entry.getValue());
			message.setContent(multiPart);
		}
	}

	/**
	 * Method to set the default text in the email.
	 * 
	 * @param message
	 *            the message object onto which the default text has to be set.
	 * @param multiPart
	 *            the multipart object into which each message part is added
	 * @throws MessagingException
	 *             throws a MessagingException if an exception occurs while
	 *             filling the message object.
	 */
	private void fillDefault(Message message, final Multipart multiPart)
			throws MessagingException {
		final BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(EmailConstants.DEFAULT_TEXT);
		multiPart.addBodyPart(messageBodyPart);
		message.setContent(multiPart);
	}

	/**
	 * Method to fill the email-body.
	 * 
	 * @param message
	 *            the email-message to be sent.
	 * 
	 * @param queryCsvMap
	 *            the map which contains the query and the file in which the
	 *            query results are written into.
	 * 
	 * @return Message the message with the contents set.
	 * 
	 * @throws MessagingException
	 *             throws an MessagingException if something bad happened when
	 *             adding the body part to the email.
	 */
	private Message fillEmailBody(Message message,
			Map<String, String> queryCsvMap) throws MessagingException {
		final Multipart multiPart = new MimeMultipart();
		fillDefault(message, multiPart);
		fillCsvData(message, queryCsvMap, multiPart);
		return message;
	}

	/**
	 * Method to return the cc address list.
	 * 
	 * @return List<Address> list of cc addresses.
	 */
	private List<Address> getCcList() {
		Element cCElement;
		try {
			cCElement = EmailXmlParser.getCcRecipients();
			final List<Address> ccAddressList = parseAddress(cCElement);
			return ccAddressList;
		} catch (final IOException e) {
			LOG.error("An exception happened while fetching the cc addresses.",
					e);
		}
		return null;
	}

	/**
	 * Method to get the email-subject.
	 * 
	 * @return emailSubject String that represents the email-subject.
	 */
	private String getEmailSubject() {
		Element subjectElement;
		try {
			subjectElement = EmailXmlParser.getEmailSubject();
			final String emailSubject = subjectElement.getTextTrim();
			return emailSubject;
		} catch (final IOException e) {
			LOG.error(
					"An exception happened while fetching the email-subject.",
					e);
		}
		return null;
	}

	/**
	 * Method to return the from-address.
	 * 
	 * @return Address email address of the sender.
	 */
	private Address getFromAddress() {
		Element fromElement;
		try {
			fromElement = EmailXmlParser.getEmailSender();
			final String fromAddress = fromElement.getTextTrim();
			final InternetAddress address = new InternetAddress(fromAddress);
			address.validate();
			return address;
		} catch (final AddressException e) {
			LOG.error(
					"No from-address found in the email.xml.Was the email.xml configured properly ???",
					e);
		} catch (final IOException e1) {
			LOG.error("An exception happened while fetching the from-address.",
					e1);
		}
		return null;
	}

	/**
	 * Method to get the host.
	 * 
	 * @return host String that represents the host where the smtp-server is
	 *         running.
	 */
	private String getHost() {
		Element hostElement;
		try {
			hostElement = EmailXmlParser.getHost();
			final String host = hostElement.getTextTrim();
			return host;
		} catch (final IOException e) {
			LOG.error("An exception happened while fetching the host.", e);
		}
		return null;
	}

	/**
	 * Method to get the port.
	 * 
	 * @return port String that represents the port number of the host.
	 */
	private String getPort() {
		Element portElement;
		try {
			portElement = EmailXmlParser.getPort();
			String port = portElement.getTextTrim();
			if (port == null || port.isEmpty()) {
				port = EmailConstants.DEFAULT_PORT;
			}
			return port;
		} catch (final IOException e) {
			LOG.error("An exception happened while fetching the port.", e);
		}
		return null;
	}

	/**
	 * Method to return the recipient address list.
	 * 
	 * @return List<Address> list of recipient addresses.
	 */
	private List<Address> getToAddressList() {
		Element toAddressElement;
		try {
			toAddressElement = EmailXmlParser.getRecipients();
			final List<Address> toAddressList = parseAddress(toAddressElement);
			if (toAddressList.isEmpty()) {
				LOG.error("No recipients found in the email.xml.Was the email.xml configured properly ???");
				throw new RuntimeException(
						"To address not configured properly.");
			}
			return toAddressList;
		} catch (final IOException e) {
			LOG.error("An exception happened while fetching the recipients.", e);
		}
		return null;
	}

	/**
	 * Method that parses the address element, retrieves the address string from
	 * the element, splits the address string and checks the validity of each of
	 * the addresses.
	 * 
	 * @param emailAddressElement
	 *            the xml element that represents the email-address.
	 * @return List<Address> list of valid email addresses.
	 */
	private List<Address> parseAddress(Element emailAddressElement) {
		InternetAddress emailAddress = null;
		final List<Address> addressList = new ArrayList<Address>();
		if (emailAddressElement != null) {
			final String addressString = emailAddressElement.getTextTrim();
			if (addressString != null && !addressString.isEmpty()) {
				final String[] addressArray = addressString
						.split(Constants.DEFAULT_EMAIL_ADDRESS_SEPARATOR);
				if (addressArray.length > 0) {
					for (final String address : addressArray) {
						try {
							emailAddress = new InternetAddress(address);
							emailAddress.validate();
							addressList.add(emailAddress);
						} catch (final AddressException e) {
							LOG.error(
									"An exception happened while parsing email addresses.",
									e);
						}
					}
				}
			}
		}
		return addressList;
	}

	/**
	 * Method to create email attachment.
	 * 
	 * @param multiPart
	 *            multi-part message object
	 * @param value
	 *            that represents the file to be attached.
	 * @return BodyPart the attachment part of the email
	 * @throws MessagingException
	 */
	private void setAttachmentPart(Multipart multiPart, final String value) {
		BodyPart attachmentPart = null;
		final File file = new File(value);
		final DataSource source = new FileDataSource(file.getAbsoluteFile());
		try {
			attachmentPart = new MimeBodyPart();
			attachmentPart.setDataHandler(new DataHandler(source));
			attachmentPart.setFileName(file.getName());
			multiPart.addBodyPart(attachmentPart);
		} catch (final MessagingException e) {
			LOG.error("An exception happened while adding attachment.", e);
		}
	}

	/**
	 * Sets the envelope content of the email message. The envelope content
	 * consists of the from and to email-address(es), cc email-addresses if any
	 * and also the subject line.
	 * 
	 * @param message
	 *            The email message content
	 * @throws MessagingException
	 */
	private void writeEnvelope(final Message message) throws MessagingException {
		// set from address
		final Address address = getFromAddress();
		if (address != null) {
			message.setFrom(getFromAddress());
		}

		// set subject
		final String emailSubject = getEmailSubject();
		if (emailSubject != null && !emailSubject.isEmpty()) {
			message.setSubject(getEmailSubject());
		} else {
			message.setSubject(EmailConstants.DEFAULT_SUBJECT);
			LOG.info("Default subject set to the email message.");
		}

		// set recipients
		final List<Address> toAddressList = getToAddressList();
		if (!toAddressList.isEmpty()) {
			for (final Address toAddress : toAddressList) {
				message.addRecipient(Message.RecipientType.TO, toAddress);
			}
		}

		// set cc addresses
		for (final Address ccAddress : getCcList()) {
			message.addRecipient(Message.RecipientType.CC, ccAddress);
		}
	}

}
