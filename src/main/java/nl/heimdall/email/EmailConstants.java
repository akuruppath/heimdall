/**
 * EmailConstants.java - constants related to email functionality.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.email;

/**
 * This class contains constants related to sending email.
 *
 * @author ajay
 *
 */

public class EmailConstants {

	public static final String DEFAULT_PORT = "25";
  public static final String DEFAULT_SUBJECT = "Report generated";
	public static final String DEFAULT_TEXT = "Your report has been generated.";

	public static final String EMAIL_CC_PATH = "//email/cc";
	public static final String EMAIL_CONFIG_HOST = "//email/configuration/host";
	public static final String EMAIL_CONFIG_PORT = "//email/configuration/port";
	public static final String EMAIL_FROM_PATH = "//email/from_address";
	public static final String EMAIL_SUBJECT_PATH = "//email/subject";
	public static final String EMAIL_TO_PATH = "//email/to_address";
	public static final String EMAIL_XML = "email.xml";

	public static final String SMTP_HOST_PROPERTY = "mail.smtp.host";
	public static final String SMTP_PORT = "mail.smtp.port";

}
