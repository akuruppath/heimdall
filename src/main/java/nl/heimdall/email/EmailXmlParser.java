/**
 * EmailXmlParser.java - range of methods related to getting info from email.xml.
 *
 * Copyright (C) 2014  Ajay Kuruppath
 * Copyright (C) 2014  West Consulting

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.email;

import main.java.nl.heimdall.utils.XmlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


import org.dom4j.Element;

/**
 * This class consists of methods that are responsible for parsing the email.xml
 * and getting information from that.
 * 
 * @author ajay
 * 
 */

public class EmailXmlParser {

	    /**
     * Method that is responsible for returning the node that contains the cc
     * address(es).
     * 
     * @return Element that contains the cc email address(es)
     * @throws IOException
     *             throws an IOException if the XML file cannot be opened
     *             properly.
     */
	public static Element getCcRecipients() throws IOException {
		return XmlUtils
				.getXmlNode(getEmailFile(), EmailConstants.EMAIL_CC_PATH);
	}

	    /**
     * Method that that is responsible for returning the email from-address.
     * 
     * @return email from-address
     * @throws IOException
     *             throws an IOException if the XML file cannot be opened
     *             properly.
     */
	public static Element getEmailSender() throws IOException {
		return XmlUtils.getXmlNode(getEmailFile(),
				EmailConstants.EMAIL_FROM_PATH);
	}

	    /**
     * Method that parses the email XML and returns the email subject.
     * 
     * @return email subject
     * @throws IOException
     *             throws an IOException if the XML file cannot be opened
     *             properly.
     */
	public static Element getEmailSubject() throws IOException {
		return XmlUtils.getXmlNode(getEmailFile(),
				EmailConstants.EMAIL_SUBJECT_PATH);
	}

	    /**
     * Method that is responsible for returning the node that contains the host.
     * 
     * @return Element that contains the host
     * @throws IOException
     *             throws an IOException if the XML file cannot be opened
     *             properly.
     */
	public static Element getHost() throws IOException {
		return XmlUtils.getXmlNode(getEmailFile(),
				EmailConstants.EMAIL_CONFIG_HOST);
	}

	    /**
     * Method that is responsible for returning the node that contains the host.
     * 
     * @return Element that contains the host
     * @throws IOException
     *             throws an IOException if the XML file cannot be opened
     *             properly.
     */
	public static Element getPort() throws IOException {
		return XmlUtils.getXmlNode(getEmailFile(),
				EmailConstants.EMAIL_CONFIG_PORT);
	}

	    /**
     * Method that is responsible for returning the node that contains the email
     * addresses.
     * 
     * @return Element that contains the email address(es)
     * @throws IOException
     *             throws an IOException if the XML file cannot be opened
     *             properly.
     */
	public static Element getRecipients() throws IOException {
		return XmlUtils
				.getXmlNode(getEmailFile(), EmailConstants.EMAIL_TO_PATH);
	}

	    /**
     * Method to return the input-stream that corresponds to the email.xml.
     * 
     * @return a stream
     */
	private static FileInputStream getEmailFile() {
		FileInputStream stream = null;
		try {
			final File jarPath = new File(XmlUtils.class.getProtectionDomain()
					.getCodeSource().getLocation().getPath());
			final String propertiesPath = jarPath.getParentFile()
					.getAbsolutePath();
			stream = new FileInputStream(propertiesPath + File.separator
					+ EmailConstants.EMAIL_XML);
		} catch (final IOException e1) {
			e1.printStackTrace();
		}
		return stream;
	}

}
