/**
 * CsvExporter.java - consists of range of methods related to generating csv files.
 *
 * Copyright (C) 2014  Ajay Kuruppath
 * Copyright (C) 2014  West Consulting

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.exporter;

import main.java.nl.heimdall.cli.CliConstants;
import main.java.nl.heimdall.cli.OptionParser;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * This class consists of methods used to export the data to CSV files.
 * 
 * @author ajay
 * 
 */

public class CsvExporter {

	private static Logger LOG = Logger.getLogger(CsvExporter.class);
	private final Charset UTF8_CHARSET = Charset.forName("UTF-8");

	    /**
     * Method to write the data from the result set to a CSV file.
     * 
     * @param rs
     *            ResultSet that contains the query results.
     * @param csvNodeValue
     *            Node from the job.xml that specifies the path to the CSV file.
     * 
     */
	public void exportToCsv(ResultSet rs, String csvNodeValue) {
		final String csvFilePath = csvNodeValue.trim();
		try {
			final CSVWriter writer = getCsvWriter(csvFilePath);
			if (writer != null) {
				writer.writeAll(rs, true);
				writer.close();
			}
		} catch (final IOException e) {
			e.printStackTrace();
		} catch (final SQLException e) {
			e.printStackTrace();
		}
		LOG.info("CSV file generated at : " + csvFilePath);
	}

	    /**
     * Method to initialise a CSVWriter with parameters.
     * 
     * @param csvFilePath
     *            the file which has to be written
     * @return writer a CSV Writer initialised with the parameters.
     */
	private CSVWriter getCsvWriter(String csvFilePath) {
		OutputStreamWriter outputStreamWriter;
		try {
			outputStreamWriter = new OutputStreamWriter(new FileOutputStream(
					csvFilePath), UTF8_CHARSET);
			CSVWriter writer = null;
			final String separatorString = OptionParser
					.getOption(CliConstants.SEPARATOR);
			final String quoteString = OptionParser
					.getOption(CliConstants.QUOTE);
			final String escapeString = OptionParser
					.getOption(CliConstants.ESCAPE);
			final String lineend = OptionParser.getOption(CliConstants.LINEEND);
			if (separatorString != null && quoteString != null
					&& escapeString != null && lineend != null) {
				writer = new CSVWriter(outputStreamWriter,
						separatorString.charAt(0), quoteString.charAt(0),
						escapeString.charAt(0), lineend);
				LOG.info("Writer initialized with separator, quote, escape chars and line-feed.");
			}
			if (writer == null) {
				LOG.info("Writer initialized with just a filewriter.");
				writer = new CSVWriter(outputStreamWriter);
			}
			return writer;
		} catch (final FileNotFoundException e) {
			System.err.println("The csv path is non-existent : " + csvFilePath);
			LOG.error("Path not found : ",e);
		}
		return null;
	}
}
