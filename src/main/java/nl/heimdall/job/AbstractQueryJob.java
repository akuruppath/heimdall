/**
 * AbstractQueryJob.java - consists of methods that initializes database related stuff.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.job;

import main.java.nl.heimdall.db.DbUtils;
import main.java.nl.heimdall.constants.Constants;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;


import org.apache.log4j.Logger;

/**
 * This class is the superclass for Job related classes. This class is primarily
 * responsible for holding the objects related to database connectivity.
 * 
 * @author ajay
 * 
 */

public class AbstractQueryJob {

	private static Logger LOG = Logger.getLogger(AbstractQueryJob.class);

	protected Connection connection;
	protected Statement statement;
	protected final Map<String, String> dbConfigMap;
	protected final DbUtils utils = new DbUtils();

	/**
	 * Constructor.
	 */
	public AbstractQueryJob() {
		dbConfigMap = DbUtils.getDbConfigMap();
		if (dbConfigMap == null || dbConfigMap.isEmpty()) {
			LOG.error("Couldn't get the database config map. Is the database configuration defined ???");
			return;
		}
		loadDbThings();
	}

	/**
	 * Method to close the database connection.
	 */
	protected void closeConnection() {
		if (connection != null) {
			try {
				LOG.info("Closing the connection now...");
				connection.close();
			} catch (SQLException e) {
				LOG.error(
						"An exception happened while closing the connection.",
						e);
			}
		}
	}

	/**
	 * Method to get the Resultset object corresponding to a query.
	 * 
	 * @param query
	 * @return resultset the resultset object corresponding to the query
	 */
	protected ResultSet getResultSet(String query) {
		ResultSet rs = null;
		if (statement != null) {
			try {
				rs = statement.executeQuery(query);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			throw new RuntimeException("Null statement object.");
		}
		return rs;
	}

	/**
	 * Method that is responsible for loading the stuff necessary to connect to
	 * the database. Initialises the driver, connection and the statement.
	 */
	protected void loadDbThings() {
		try {
			Class.forName(dbConfigMap.get(Constants.JDBC_DRIVER));
			LOG.debug("Loaded the jdbc driver.");
			connection = DriverManager.getConnection(
					dbConfigMap.get(Constants.JDBC_URL),
					dbConfigMap.get(Constants.JDBC_USERNAME),
					dbConfigMap.get(Constants.JDBC_PASSWORD));
			LOG.debug("Got connection object : " + connection);
			if (connection != null) {
				connection.setAutoCommit(false);
				statement = connection.createStatement();
			} else {
				LOG.error("Couldn't get connection object.");
			}
		} catch (final ClassNotFoundException e) {
			System.err
					.println("Driver not found !! Check logs for the full stacktrace.");
			LOG.fatal("Driver not found !! ", e);
		} catch (final SQLException e) {
			LOG.fatal("Error connecting to the database !! ", e);
		}
	}

}
