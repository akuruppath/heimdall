/**
 * Job.java - range of methods related to execution of database queries and generating CSV files.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.job;

import main.java.nl.heimdall.exporter.CsvExporter;
import main.java.nl.heimdall.constants.Constants;
import main.java.nl.heimdall.cli.CliConstants;
import main.java.nl.heimdall.cli.OptionParser;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import main.java.nl.heimdall.email.Email;

import org.apache.log4j.Logger;
import org.dom4j.Element;
import org.dom4j.Node;

/**
 * Class which is responsible for executing the queries specified in the job.xml
 * and generates a CSV file for each query.
 * 
 * @author ajay
 * 
 */

public class Job extends AbstractQueryJob {

	private static Logger LOG = Logger.getLogger(Job.class);
	private static final String FILE_SEPARATOR = Pattern.quote(System
			.getProperty("file.separator"));

	/**
	 * Method to get the iterator node that contains all the queries. This
	 * method uses helper methods to extract queries and to execute them.
	 */
	public void execute() {
		Iterator<? extends Node> iterator;
		try {
			iterator = JobXmlParser.getAllQueries();
			if (iterator == null) {
				LOG.error("No queries found !!! Are the query nodes defined ???");
				return;
			}
			final Map<String, String> queryCsvMap = extractQueryAndExecute(iterator);
			final String emailOptionSet = OptionParser
					.getOption(CliConstants.EMAIL);
			if (emailOptionSet != null) {
				final boolean emailOn = emailOptionSet
						.equalsIgnoreCase(CliConstants.EMAIL_OPT_YES);
				if (!queryCsvMap.isEmpty() && emailOn) {
					new Email().sendEmail(queryCsvMap);
				}
			}
		} catch (final IOException e) {
			LOG.error("An exception happened while fetching queries.", e);
		}
	}

	/**
	 * Method to check if a base name exists.
	 * 
	 * @param csvNodeValue
	 * @return true if a base-name for the CSV file exists else false.
	 */
	private boolean baseNameExists(String csvNodeValue) {
		// use zero-width positive lookahead:
		// http://stackoverflow.com/questions/4545937/java-splitting-the-filename-into-a-base-and-extension
		boolean baseNameExists = false;
		final File f = new File(csvNodeValue);
		final String[] baseNameTokens = f.getName().split("\\.(?=[^\\.]+$)");
		if (baseNameTokens.length > 1) {
			baseNameExists = baseNameTokens[1].equalsIgnoreCase("csv");
		}
		return baseNameExists;
	}

	/**
	 * Method to determine the path and the CSV file to be generated.
	 * 
	 * <p>
	 * If CSV file name is not defined inside the current node, then the CSV
	 * will be generated in the default location : the class-path.
	 * </p>
	 * 
	 * @param csvNodeValue
	 *            the value specified in the CSV node of the job.xml
	 * @return csvNodeValue the new value of the path and the file depending on
	 *         the value of the parameter csvNodeValue
	 */
	private String determineCsvPath(String csvNodeValue) {
		if (csvNodeValue == null || csvNodeValue.isEmpty()
				|| !baseNameExists(csvNodeValue)) {
			LOG.info("No csv file and path specified. The file generated will be "
					+ Constants.DEFAULT_CSV_FILE);
			csvNodeValue = Constants.DEFAULT_CSV_FILE;
		} else {
			if (csvNodeValue.split(FILE_SEPARATOR).length == 1) {
				LOG.info("No path specified. Will generate csv file in "
						+ Constants.DEFAULT_CSV_PATH);
				csvNodeValue = Constants.DEFAULT_CSV_PATH + csvNodeValue;
			}
		}
		return csvNodeValue;
	}

	/**
	 * Method that executes the queries specified in the job.xml and writes the
	 * result to a CSV file.This method iterates over all the query nodes,
	 * extract the query in each of these nodes, execute them and then write the
	 * result into a CSV file specified.
	 * <p>
	 * If a query CDATA node is not defined, that node will be skipped and the
	 * execution will continue with the next query node.
	 * </p>
	 * 
	 * @param iterator
	 *            the iterator that iterates over the query elements.
	 * 
	 * @return Map<String, String> a map with the query and the file which
	 *         contains the results of the query.
	 */
	private Map<String, String> extractQueryAndExecute(
			final Iterator<? extends Node> iterator) {
		final CsvExporter csvExporter = new CsvExporter();
		final Map<String, String> queryCsvMap = new HashMap<String, String>();
		while (iterator.hasNext()) {
			final Element queryElement = (Element) iterator.next();
			final String query = queryElement.elementText(Constants.NODE_CODE)
					.trim();
			if (JobUtils.isEmptyQueryString(query)
					|| JobUtils.isMultiQueryString(query)
					|| JobUtils.isIllegalQueryString(query)) {
				LOG.error("Illegal query !!! Skipping the current node and continuing with the next.");
				continue;
			}
			try {
				LOG.info("Running query : " + query);
				final ResultSet rs = getResultSet(query);
				if (rs != null) {
					String csvNodeValue = queryElement.elementText(
							Constants.NODE_CSV_NAME).trim();
					csvNodeValue = determineCsvPath(csvNodeValue);
					csvExporter.exportToCsv(rs, csvNodeValue);
					queryCsvMap.put(query, csvNodeValue);
					rs.close();
				}
			} catch (final SQLException e) {
				LOG.error("An exception happened while query execution.", e);
			}
		}
		closeConnection();
		return queryCsvMap;
	}
}
