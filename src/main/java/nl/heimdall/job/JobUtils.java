/**
 * JobUtils.java - contains related methods for job related utilities.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.job;

/**
 * This class contains methods which act as helpers to the Job class related
 * functionality.
 * 
 * @author ajay
 * 
 */

public class JobUtils {

	private static final String ALTER = "ALTER";
	private static final String CREATE = "CREATE";
	private static final String DELETE = "DELETE";
	private static final String DROP = "DROP";
	private static final String SELECT = "SELECT";
	private static final String TRUNCATE = "TRUNCATE";
	private static final String UPDATE = "UPDATE";

	private static final String REGEX_SPACE = "\\s+";

	/**
	 * Method to check if the query string is a illegal. The query string is
	 * considered to be illegal if:
	 * <p>
	 * 1. the string is null or 2. the length is 0 or 3. it is empty.
	 * </p>
	 * 
	 * @param query
	 * @return true if it fulfils any of the above three conditions, false
	 *         otherwise.
	 */
	public static boolean isEmptyQueryString(String query) {
		return query == null || query.length() == 0 || query.isEmpty();
	}

	/**
	 * Method to check if the query is an illegal query. A query is considered
	 * to be an illegal query in the two cases:
	 * <p>
	 * 1. if it has a create, delete or an update substring anywhere in the
	 * query string. 2. if it is not a select query.
	 * </p>
	 * 
	 * @param query
	 * @return true if the query is an illegal query. false otherwise.
	 */
	public static boolean isIllegalQueryString(String query) {
		String queryString = query.toUpperCase();
		String[] queryPieces = queryString.split(REGEX_SPACE);
		return queryString.indexOf(ALTER) > -1
				|| queryString.indexOf(CREATE) > -1
				|| queryString.indexOf(DELETE) > -1
				|| queryString.indexOf(DROP) > -1
				|| queryString.indexOf(TRUNCATE) > -1
				|| queryString.indexOf(UPDATE) > -1
				|| !queryPieces[0].equals(SELECT);
	}

	/**
	 * Method to check if more than one query is specified in a single query
	 * string separated by delimiter.
	 * 
	 * @param query
	 * @return true if there are queries separated by delimiter and false
	 *         otherwise.
	 */
	public static boolean isMultiQueryString(String query) {
		return query.split("[;]+").length > 1;
	}
}
