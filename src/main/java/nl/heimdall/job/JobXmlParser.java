/**
 * JobXmlParser.java - range of methods related to getting info from job.xml.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.job;

import main.java.nl.heimdall.utils.XmlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;


import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 * This class consists of methods that are responsible for parsing the job.xml
 * and getting information from that.
 * 
 * @author ajay
 * 
 */

public class JobXmlParser {

	private static Logger LOG = Logger.getLogger(JobXmlParser.class);

	private static final String JOB_XML = "job.xml";
	private static final String QUERY_PATH = "//job/queries/query";

	/**
	 * Method that parses the job XML and returns the query nodes in an
	 * iterator.
	 * 
	 * @return Iterator with all the query nodes
	 * @throws IOException
	 *             throws an IOException if an error occurred in parsing the
	 *             XML.
	 */
	public static Iterator<? extends Node> getAllQueries() throws IOException {
		FileInputStream xmlStream = null;
		try {
			xmlStream = getJobFile();
			LOG.trace("Read stream :" + JOB_XML);
			final SAXReader saxReader = new SAXReader();
			final Document document = saxReader.read(xmlStream);
			LOG.trace("Got document object from xmlstream.");
			@SuppressWarnings("unchecked")
			final Iterator<? extends Node> queryIterator = document
					.selectNodes(QUERY_PATH).iterator();
			if (!queryIterator.hasNext()) {
				LOG.error(QUERY_PATH + "not found !!! Please define the "
						+ QUERY_PATH + " properly in " + JOB_XML);
				throw new IllegalArgumentException(QUERY_PATH + " not defined.");
			}
			return queryIterator;
		} catch (final DocumentException e) {
			e.printStackTrace();
		} finally {
			if (xmlStream != null) {
				xmlStream.close();
			}
		}
		return null;
	}

	/**
	 * Method to return the input-stream that corresponds to the job.xml.
	 * 
	 * @return stream a stream
	 */
	private static FileInputStream getJobFile() {
		FileInputStream stream = null;
		try {
			final File jarPath = new File(XmlUtils.class.getProtectionDomain()
					.getCodeSource().getLocation().getPath());
			final String propertiesPath = jarPath.getParentFile()
					.getAbsolutePath();
			stream = new FileInputStream(propertiesPath + File.separator
					+ JOB_XML);
		} catch (final IOException e1) {
			e1.printStackTrace();
		}
		return stream;
	}
}
