/**
 * XmlParser.java - consists of range of methods that parses the XML files.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package main.java.nl.heimdall.utils;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * Class that contains the methods required to parse the XML files and returns
 * the necessary nodes.
 * 
 * @author ajay
 * 
 */

public class XmlUtils {

	private static Logger LOG = Logger.getLogger(XmlUtils.class);

	/**
	 * Method to parse the XML file represented by the xmlFileName and retrieve
	 * the contents of the Xpath argument.
	 * 
	 * @param fileInputStream
	 *            the name of the XML file to be parsed.
	 * @param xPath
	 *            the path to the element being parsed.
	 * @return Element represented by the xPath
	 * @throws IOException
	 *             throws an IOException if the XML file cannot be opened
	 *             properly.
	 */
	public static Element getXmlNode(FileInputStream fileInputStream,
			String xPath) throws IOException {
		FileInputStream xmlStream = null;
		try {
			xmlStream = fileInputStream;
			LOG.trace("Read stream :" + fileInputStream);
			final SAXReader saxReader = new SAXReader();
			final Document document = saxReader.read(xmlStream);
			LOG.trace("Got document object from xmlstream.");
			final Element element = (Element) document.selectSingleNode(xPath);
			if (element == null) {
				LOG.error(xPath + "not found !!! Please define the " + xPath
						+ " properly in " + fileInputStream);
				throw new IllegalArgumentException(xPath + " not defined.");
			}
			return element;
		} catch (final DocumentException e) {
			e.printStackTrace();
		} finally {
			if (xmlStream != null) {
				xmlStream.close();
			}
		}
		return null;
	}

}
