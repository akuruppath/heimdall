/**
 * GenerateCSVTest.java - tests related to generating a CSV file.
 *
 * Copyright (C) 2014  Ajay Kuruppath
 * Copyright (C) 2014  West Consulting

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package test.java.nl.heimdall.cli;

import main.java.nl.heimdall.cli.CliConstants;
import main.java.nl.heimdall.cli.OptionParser;
import java.io.File;


import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

public class GenerateCSVTest {

	// try to see if the arguments passed are being loaded properly.
	@Test
	public void parseReportOptions() throws Exception {
		final String[] args = { "--dbtype", "h2", "--separator", "@",
				"--quote", "single", "--escape", "/", "--lineending", "unix" };
		OptionParser.main(args);
		Assert.assertEquals("h2", OptionParser.getOption(CliConstants.DBTYPE));
		Assert.assertEquals("@", OptionParser.getOption(CliConstants.SEPARATOR));
		Assert.assertEquals("'", OptionParser.getOption(CliConstants.QUOTE));
		Assert.assertEquals("/", OptionParser.getOption(CliConstants.ESCAPE));
		Assert.assertEquals("\n", OptionParser.getOption(CliConstants.LINEEND));
		final File test1 = new File(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test1.csv");
		final File test2 = new File(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test2.csv");
		Assert.assertEquals("test1.csv not generated.", "test1.csv",
				test1.getName());
		Assert.assertEquals("test2.csv not generated.", "test2.csv",
				test2.getName());
	}

	// try to see if default values are being loaded properly.
	@Test
	public void loadDefaultOptions() throws Exception {
		final String[] args = { "--dbtype", "h2" };
		OptionParser.main(args);
		Assert.assertEquals("h2", OptionParser.getOption(CliConstants.DBTYPE));
		Assert.assertEquals("no", OptionParser.getOption(CliConstants.EMAIL));
		Assert.assertEquals(",", OptionParser.getOption(CliConstants.SEPARATOR));
		Assert.assertEquals("\"", OptionParser.getOption(CliConstants.QUOTE));
		Assert.assertEquals("/", OptionParser.getOption(CliConstants.ESCAPE));
		Assert.assertEquals("\n", OptionParser.getOption(CliConstants.LINEEND));
		final File test1 = new File(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test1.csv");
		final File test2 = new File(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test2.csv");
		Assert.assertEquals("test1.csv not generated.", "test1.csv",
				test1.getName());
		Assert.assertEquals("test2.csv not generated.", "test2.csv",
				test2.getName());
	}

	@AfterClass
	public static void clean() {
		final File test1 = new File(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test1.csv");
		if (test1.exists()) {
			test1.delete();
		}
		final File test2 = new File(System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "test2.csv");
		if (test2.exists()) {
			test2.delete();
		}
	}
}
