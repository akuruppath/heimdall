/**
 * JDbReportOptionsTest.java - consists of tests for ReportOptions class.
 *
 * Copyright (C) 2014  Ajay Kuruppath
 * Copyright (C) 2014  West Consulting

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package test.java.nl.heimdall.cli;

import main.java.nl.heimdall.cli.ReportOptions;

import org.apache.commons.cli.Options;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author ajay
 * 
 */
public class JDbReportOptionsTest {

	/**
	 * Test method for
	 * {@link ReportOptions.heimdall.cli.ReportOptions#getOptions()}.
	 */
	@Test
	public void testGetJdbReportOptions() {
		final Options jdbReportOptions = new ReportOptions()
				.getOptions();
		Assert.assertEquals(8, jdbReportOptions.getOptions().size());
	}

}
