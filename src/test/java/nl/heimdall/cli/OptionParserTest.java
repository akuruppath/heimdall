/**
 * OptionParserTest.java - consists of tests for OptionParser class.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package test.java.nl.heimdall.cli;

import main.java.nl.heimdall.cli.CliConstants;
import main.java.nl.heimdall.cli.OptionParser;
import java.io.File;


import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author ajay
 * 
 */
public class OptionParserTest {

	// try to run without passing the mandatory dbtype argument.
	@Test(expected = IllegalArgumentException.class)
	public void noDbOption() throws Exception {
		final String[] args = { "--quote", "'" };
		OptionParser.main(args);
	}

	// try to run by passing an unsupported db type.
	@Test(expected = IllegalArgumentException.class)
	public void unSupportedDbOption() throws Exception {
		final String[] args = { "--dbtype", "mariadb" };
		OptionParser.main(args);
	}

	// try to run by passing an unknown argument.
	@Test(expected = IllegalArgumentException.class)
	public void unRecognizedOption() throws Exception {
		final String[] args = { "--dbtype", "h2", "--oranges", "5" };
		OptionParser.main(args);
	}
}
